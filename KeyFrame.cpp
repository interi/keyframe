#include "KeyFrame.h"

KF_NAMESPACE_S

KeyFrame::KeyFrame(Transport& transport, Callback& callback) {
  callback_  = &callback;
  transport_ = &transport;
  callback_ ->keyframe_ = this;
  transport_->keyframe_ = this;
  dcdd_ = kStateInt;
}

uint8_t* KeyFrame::sendBuffer(void) {
  return send_buff_;
}

Status KeyFrame::send(unsigned int length) {
  // Calculate the message checksum
  crc_send_ = crc_update(crc_init(), send_buff_, length);
  crc_send_ = crc_finalize(crc_send_);
  crc_byte_ = static_cast<byte*>(&crc_send_);

  // Start every message with an END character (to flush the line)
  encd_buff_[0] = KEYFRAME_SLIP_END;

  // Encode the payload body (including the CRC)
  l_ = length + sizeof(crc_t);
  for(i_ = 0, j_ = 1; i_ < l_; ++i_, ++j_) {
      p_send_ = (i_ < length) ? send_buff_[i_] : crc_byte_[i_-length];
      switch(p_send_) {
        case KEYFRAME_SLIP_END: // Escape END characters
          encd_buff_[  j_] = KEYFRAME_SLIP_ESC;
          encd_buff_[++j_] = KEYFRAME_SLIP_END_ESC;
          break;
        case KEYFRAME_SLIP_ESC: // Escape ESC characters
          encd_buff_[  j_] = KEYFRAME_SLIP_ESC;
          encd_buff_[++j_] = KEYFRAME_SLIP_ESC_ESC;
          break;
        default: // Include everything else
          encd_buff_[  j_] = p_send_;
          break;
      }
  }

  // Complete the payload with an END character
  encd_buff_[j_++] = KEYFRAME_SLIP_END;

  // Send the payload
  return transport_->send(encd_buff_, j_);
}

Status KeyFrame::recv(unsigned int length) {
  Status status = kStatusOk;
  byte* payload = transport_->recvBuffer();
  for(k_ = 0; k_ < length; ++k_) {
    // Get the next byte
    p_recv_ = payload[k_];
    
    // Decode the next byte
    switch(dcdd_) {
      case kStateErr: // If there was an error, read to next END
      case kStateInt: // Read until first END encoundered
        switch(p_recv_) {
          case KEYFRAME_SLIP_END:
            // Move the buffer to the front to init
            // This is a fast operation, so its alright if we do this a bunch
            dcdd_idx_ = 0;

            // Pivot again to handle back to back END characters
            dcdd_ = (dcdd_ == kStateErr) ? kStateInt : kStateBeg;
            break;
          default: break;
        }
        break;
      case kStateMsg: // Read the body of the payload
        switch(p_recv_) {
          case KEYFRAME_SLIP_END: // We've hit the end of the payload
            dcdd_ = kStateEnd;
            break;
          case KEYFRAME_SLIP_ESC: // There is an escaped character next
            dcdd_ = kStateEsc;
            break;
          default: break;
        }
        break;
      case kStateEsc: // Handle escaped characters
        switch(p_recv_) {
          case KEYFRAME_SLIP_END_ESC: // ENDESC => END
            p_recv_ = KEYFRAME_SLIP_END;
            dcdd_ = kStateMsg;
            break;
          case KEYFRAME_SLIP_ESC_ESC: // ESCESC => ESC
            p_recv_ = KEYFRAME_SLIP_ESC;
            dcdd_ = kStateMsg;
            break;
          default: // Everything else is totally wrong
            dcdd_ = kStateErr;
            break;
        }
        break;
      default: break;
    }

    // Buffer between decodes
    switch(dcdd_) {
      case kStateBeg: // Consume the END character at the beginning of the payload
        dcdd_ = kStateMsg;
        break;
      case kStateErr: // We got an error while decoding the payload
        // Report encoding error
        callback_->error(kErrEncoding);
        status = kErrReceive;
        break;
      case kStateEnd: // We've hit the end of the payload
        // Calculate payload checksum
        crc_recv_ = crc_update(crc_init(), dcdd_buff_, dcdd_idx_);
        crc_recv_ = crc_finalize(crc_recv_);

        // Handle checksum result
        if(crc_recv_ == 0) {
          // Silently squash empty messages
          if(dcdd_idx_ > 0)
            callback_->receive(dcdd_buff_, dcdd_idx_-sizeof(crc_t));
        } else {
          // Report checksum error
          callback_->error(kErrChecksum);
          status = kErrReceive;
        }

        // Reset back to beginning state
        dcdd_ = kStateInt;
        break;
      case kStateMsg: // Store the payload byte
        // Put the byte into the decode buffer
        dcdd_buff_[dcdd_idx_++] = p_recv_;
        break;
      default: break;
    }

    // Handle overflow - payload has a maximum length
    if(dcdd_idx_ >= KEYFRAME_MAX_PAYLOAD_LEN) {
      callback_->error(kErrOverflow);
      status = kErrReceive;
      dcdd_ = kStateErr;
    }
  }

  // Return the overall status (kErrReceive if any message errored)
  return status;
}

Status KeyFrame::step(void) {
  return transport_->step();
}

Transport::~Transport() {}

#ifdef ARDUINO
Status KeyFrame::begin(void) {
  return transport_->begin();
}

Status Transport::begin(void) { return kStatusOk; }
#endif

KF_NAMESPACE_E
