#include <KeyFrame.h>

class EchoCallback : public Callback {
public:
  virtual ~EchoCallback() {}
  
  virtual void receive(byte payload[], unsigned int length) {
    byte* buff = sendBuffer();
    buff[0] = 10;
    memcpy(buff+1, payload, length);
    send(length+1);
  }
  
  virtual void error(Status code) {
    byte* buff = sendBuffer();
    buff[0] = 20;
    buff[1] = code;
    send(2); 
  }
};

EchoCallback cb;
SerialTransport transport(Serial);
KeyFrame kf(transport, cb);

void setup() {
  Serial.begin(9600);
  kf.begin();
}

void loop() {
  kf.step();
  delay(100);
}
