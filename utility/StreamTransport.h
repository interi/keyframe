#ifndef KEYFRAME_STREAMTRANSPORT_H
#define KEYFRAME_STREAMTRANSPORT_H

template<int N>
class StreamTransport : public Transport {
public:
  StreamTransport<N>(Stream& stream) {
    stream_ = &stream;
  }

  virtual Status begin(void) {
#ifdef KEYFRAME_DEBUG
    dbgSerial.begin(9600);
    dbgSerial.println("ARDUINO - Debug Serial enabled");
#endif
    // Arduino resets on DTR, so we need to resync both sides
    // Best way so far has been to have the Arduino spam writes
    // until the other end ACKs.

    // Constantly write HELO characters while waiting
    // for an EHLO character from the other side
    length_ = 0;
    while(length_ < 1) {
      stream_->write(KEYFRAME_HELO);
      stream_->flush(); // Flush here since we're waiting for a response
      length_ = stream_->available();
    }

    // Clear buffer of EHLO characters
    while(length_ > 0) {
      stream_->read();
      length_ = stream_->available();
    }

    return kStatusOk;
  }

  virtual byte* recvBuffer(void) {
    return recv_buff_;
  }

  virtual Status send(byte* payload, unsigned int length) {
    unsigned int result = stream_->write(payload, length);
    return (result == length) ? kStatusOk : kErrTransmit;
  }

  virtual Status step(void) {
    status_ = kStatusOk;
    length_ = stream_->available();
    if(length_ > 0) {
      length_ = stream_->readBytes((char*) recv_buff_, length_);

#ifdef KEYFRAME_DEBUG
      // Output the received payload for debugging (still encoded)
      dbgSerial.println("Got Bytes:");
      int i; for(i = 0; i < length_; ++i) {
        dbgSerial.print(recv_buff_[i], DEC);
        if(i < (length_-1)) dbgSerial.print(",");
      }
      dbgSerial.println();
#endif

      if(length_ > 0) status_ = keyframe_->recv(length_);
      else status_ = kErrReceive;
    }
    return status_;
  }
protected:
  int length_;
  Status status_;
  Stream* stream_;
  byte recv_buff_[N];
};

#endif