#ifndef KEYFRAME_SERIALTRANSPORT_H
#define KEYFRAME_SERIALTRANSPORT_H

// This will need to be updated if the HardwareSerial constant ever changes
// Why it isn't in the header baffles me
#ifndef SERIAL_BUFFER_SIZE
#define SERIAL_BUFFER_SIZE 64
#endif

class SerialTransport : public StreamTransport<SERIAL_BUFFER_SIZE> {
public:
  SerialTransport(Stream& stream) 
  : StreamTransport<SERIAL_BUFFER_SIZE>(stream) {}
};

#endif