#include "KeyFrameTest.h"
#include <iostream>

class PrintCallback : public KeyFrame::Callback {
public:
  virtual void receive(byte* payload, unsigned int length) {
    std::cout << "Got Payload:" << std::endl;
    for(unsigned int i = 0; i < length; i++) {
      std::cout << (int) payload[i];
      if(i < (length-1)) std::cout << ", ";
    }
    std::cout << std::endl;
  }

  virtual void error(KeyFrame::Status code) {
    std::cout << "Got Error: " << code << std::endl;
  }
};

int main(int argc, char** argv) {
  PrintCallback cb;
  KeyFrame::SerialTransport transport("/dev/ttyACM0", 9600);
  KeyFrame::KeyFrame kf(transport, cb);

  byte* send = kf.sendBuffer();
  byte i = 0;
  while(true) {
    send[0] = (i++ % 255);
    kf.send(1);

    kf.step();
  }

  return 0;
}
