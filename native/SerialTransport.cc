#include "SerialTransport.h"
#include <iostream>

KF_NAMESPACE_S

SerialTransport::SerialTransport(std::string port, unsigned int baud,
                                 unsigned int bits, unsigned int stop) {
  // Get a reference to the port
  if(sp_get_port_by_name(port.c_str(), &port_) != SP_OK) return;

  // Open the port
  if(sp_open(port_, (sp_mode) (SP_MODE_READ | SP_MODE_WRITE)) != SP_OK) return;

  // Generate the port configuration
  sp_port_config* config;
  if(sp_new_config(&config) != SP_OK) return;
  if(sp_set_config_baudrate(config, baud) != SP_OK) return;
  if(sp_set_config_flowcontrol(config, SP_FLOWCONTROL_NONE) != SP_OK) return;
  if(sp_set_config_dtr(config, SP_DTR_OFF) != SP_OK) return;
  if(sp_set_config_parity(config, SP_PARITY_NONE) != SP_OK) return;
  if(sp_set_config_bits(config, bits) != SP_OK) return;
  if(sp_set_config_stopbits(config, stop) != SP_OK) return;

  // Update the configuration
  bool err_config = sp_set_config(port_, config) != SP_OK;

  // Clean up the configuration
  sp_free_config(config);
  if(err_config) return;

  // Configure the read event
  if(sp_new_event_set(&read_event_) != SP_OK) return;
  if(sp_add_port_events(read_event_, port_, SP_EVENT_RX_READY) != SP_OK) return;

  // Configure write event
  if(sp_new_event_set(&write_event_) != SP_OK) return;
  if(sp_add_port_events(write_event_, port_, SP_EVENT_TX_READY) != SP_OK) return;

  // Wait for the initial write (since Arduino has DTR reset)
  sp_wait(read_event_, 0);
  sp_flush(port_, SP_BUF_BOTH);

  // Write EHLO
  byte ehlo = KEYFRAME_EHLO;
  sp_blocking_write(port_, &ehlo, 1, 0); // EHLO

  sp_flush(port_, SP_BUF_BOTH);
}

SerialTransport::~SerialTransport() {
  if(port_ != NULL) {
    // Free the events
    sp_free_event_set(read_event_);
    sp_free_event_set(write_event_);

    // Drain the port
    sp_drain(port_);

    // Close the port
    sp_close(port_);
  }

  // Free the port
  sp_free_port(port_);
}

//static int test_count = 0;

Status SerialTransport::send(byte *payload, unsigned int length) {
  if(port_ == NULL) return kErrNotInit;

  // Wait until we can write
  if(sp_wait(write_event_, 0) != SP_OK) return kErrTransmit;

  // Send the payload (blocking)
  int sent = sp_blocking_write(port_, payload, length, 0);

  //std::cout << std::endl << "Send: " << test_count++ << std::endl << std::endl;

  // If it didn't completely send, clear the output buffer
  if(sent != length) {
    sp_flush(port_, SP_BUF_OUTPUT);
    return kErrTransmit;
  }
  // Otherwise, wait until the message is fully sent
  else
    return (sp_drain(port_) == SP_OK) ? kStatusOk : kErrTransmit;
}

Status SerialTransport::step(void) {
  if(port_ == NULL) return kErrNotInit;

  // XXX: This breaks everything, broken interrupt loop in libserialport, perhaps?
  // Wait until we can read
  // if(sp_wait(read_event_, 0) != SP_OK) return kErrReceive;

  // Get the number of bytes we can read
  int length = sp_input_waiting(port_);
  if(length > 0) {
    // Read that many bytes without blocking
    length = sp_nonblocking_read(port_, recv_buff_, length);

    // If we actually get bytes, process them
    if(length > 0) {
      return keyframe_->recv(length);
    } else return kErrReceive;
  }

  // XXX: DO NOT FLUSH THE BUFFER
  // sp_flush(port_, SP_BUF_INPUT);

  return kStatusOk;
}

byte* SerialTransport::recvBuffer() { return recv_buff_; }

KF_NAMESPACE_E
