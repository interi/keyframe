#ifndef KEYFRAME_SERIALTRANSPORT_H
#define KEYFRAME_SERIALTRANSPORT_H

#include <string>
#include <vector>

#include "KeyFrame.h"
#include "libserialport.h"

#ifndef NATIVE_SERIAL_BUFFER_SIZE
#define NATIVE_SERIAL_BUFFER_SIZE 1024
#endif

KF_NAMESPACE_S

class SerialTransport : public Transport {
public:
  SerialTransport(std::string port, unsigned int baud,
                  unsigned int bits = 8,
                  unsigned int stop = 1);
  virtual ~SerialTransport();

  std::vector<std::string> ports(void);

  virtual byte* recvBuffer(void);
  virtual Status send(byte* payload, unsigned int length);
  virtual Status step(void);
private:
  sp_port* port_;
  sp_event_set* read_event_;
  sp_event_set* write_event_;
  byte recv_buff_[NATIVE_SERIAL_BUFFER_SIZE];
};

KF_NAMESPACE_E

#endif
