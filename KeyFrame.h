#ifndef KEYFRAME_KEYFRAME_H
#define KEYFRAME_KEYFRAME_H

#ifndef KEYFRAME_MAX_PAYLOAD_LEN
#define KEYFRAME_MAX_PAYLOAD_LEN 10
#endif

#ifdef ARDUINO
  #include <Arduino.h>
  #define KF_NAMESPACE_S
  #define KF_NAMESPACE_E
#else
  #include <stdint.h>
  #define KF_NAMESPACE_S namespace KeyFrame {
  #define KF_NAMESPACE_E }

  typedef uint8_t byte;
#endif

#include "utility/CRC.h"

#define KEYFRAME_MAX_ENCODED_LEN (2*(KEYFRAME_MAX_PAYLOAD_LEN+sizeof(crc_t))+2)

#define KEYFRAME_HELO 104
#define KEYFRAME_EHLO 72

#define KEYFRAME_SLIP_END 192
#define KEYFRAME_SLIP_ESC 219
#define KEYFRAME_SLIP_END_ESC 220
#define KEYFRAME_SLIP_ESC_ESC 221

KF_NAMESPACE_S

enum Status {
  kStatusOk = 0,
  kErrTransmit,
  kErrReceive,
  kErrEncoding,
  kErrChecksum,
  kErrNotInit,
  kErrOverflow,
  kStatusLen
};

enum DecodeState {
  kStateInt = 0,
  kStateBeg,
  kStateMsg,
  kStateEsc,
  kStateEnd,
  kStateErr,
  kStateLen
};

class Callback;
class Transport;

class KeyFrame {
public:
  KeyFrame(Transport& transport, Callback& callback);

  byte*  sendBuffer(void);
  Status send(unsigned int length);
  Status recv(unsigned int length);
  Status step(void);

#ifdef ARDUINO
  Status begin(void); // Delegate to transport
#endif

private:
  byte send_buff_[KEYFRAME_MAX_PAYLOAD_LEN]; // User copies data in
  byte dcdd_buff_[KEYFRAME_MAX_PAYLOAD_LEN]; // Keyframe decodes transport data
  byte encd_buff_[KEYFRAME_MAX_ENCODED_LEN]; // Keyframe encodes data for transport

  unsigned int dcdd_idx_;
  DecodeState  dcdd_;

  Callback* callback_;
  Transport* transport_;

  // Pre-allocate everything used by the main class
  unsigned int i_, j_, k_, l_;
  byte p_send_, p_recv_, *crc_byte_;
  crc_t crc_send_;
  crc_t crc_recv_;
};

class Callback {
public:
  virtual ~Callback() {}
  virtual void receive(byte payload[], unsigned int length) {}
  virtual void error  (Status code) {}

protected:
  // Proxy KeyFrame methods to make a nice user API
  byte* sendBuffer() { return keyframe_->sendBuffer(); }
  Status send(unsigned int length) { return keyframe_->send(length); }

private:
  KeyFrame* keyframe_;
  friend class KeyFrame;
};

class Transport {
public:
  virtual ~Transport();
  virtual byte* recvBuffer(void) = 0;
  virtual Status send(byte* payload, unsigned int length) = 0;
  virtual Status step(void) = 0;

#ifdef ARDUINO
  virtual Status begin(void);
#endif

protected:
  KeyFrame* keyframe_;
  friend class KeyFrame;
};

#ifdef ARDUINO
  #ifdef KEYFRAME_DEBUG
    #include <SoftwareSerial.h>
    extern SoftwareSerial dbgSerial;
  #endif

  #include "utility/StreamTransport.h"
  #include "utility/SerialTransport.h"
#endif

KF_NAMESPACE_E

#endif
